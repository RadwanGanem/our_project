﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class LoggedInForm : Form
    {
        public static string userConnected { get; set; }
        public LoggedInForm(string username)
        {
            InitializeComponent();
            userConnected = username;
            usernameLabel.Text = userConnected;
            usernameHelloLabel.Text = Constants.HELLO + userConnected;
        }

        private void logOutButton_Click(object sender, EventArgs e)
        {
            MainScreen.MSG = Constants.SIGNOUT;
            MainScreen.sendMSG();
            this.Hide();
        }

        public static void recivesRooms()
        {
            MainScreen.sendMSG();
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 1000);
            RoomsForm.roomsMsg = new ASCIIEncoding().GetString(bufferIn);
        }

        private void joinRoom_Click(object sender, EventArgs e)
        {
            try
            {
                MainScreen.MSG = Constants.ROOM_REQUEST;
                recivesRooms();
                this.Hide();
                Form joinRoom = new RoomsForm();
                joinRoom.ShowDialog();
                this.Show();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void createRoom_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                Form ceateRoom = new CreateRoomForm();
                ceateRoom.ShowDialog();
                this.Show();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            MainScreen.MSG = Constants.EXIT;
            Application.Exit();
        }

        private void stats_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainScreen.MSG = Constants.MY_STATS;
            MainScreen.sendMSG();
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 23);
            string myStatsToRecive = new ASCIIEncoding().GetString(bufferIn);
            MyStatsForm.games = (Convert.ToInt32(myStatsToRecive.Substring(3,4))).ToString();
            MyStatsForm.rightAnswers = (Convert.ToInt32(myStatsToRecive.Substring(7, 6))).ToString();
            MyStatsForm.WrongAnswers = MyStatsForm.rightAnswers = (Convert.ToInt32(myStatsToRecive.Substring(13, 6))).ToString();
            MyStatsForm.time = MyStatsForm.rightAnswers = (Convert.ToSingle((myStatsToRecive.Substring(19, 2))+"."+(myStatsToRecive.Substring(21, 2)))).ToString();
            Form myStats = new MyStatsForm();
            myStats.ShowDialog();
            this.Show();
        }

        private void bestScoresButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainScreen.MSG = Constants.BEST_SCORES;
            MainScreen.sendMSG();
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 150);
            BestScoresForm.scoresMsg = new ASCIIEncoding().GetString(bufferIn);
            Form bestScores = new BestScoresForm();
            bestScores.ShowDialog();
            this.Show();
        }
    }
}
