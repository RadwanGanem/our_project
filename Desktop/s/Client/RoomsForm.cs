﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class RoomsForm : Form
    {
        public static string roomsMsg { get; set; }
        public RoomsForm()
        {
            InitializeComponent();
            MainScreen.MSG = Constants.ROOM_REQUEST;
            LoggedInForm.recivesRooms();
            usernameLabel.Text = LoggedInForm.userConnected;
            RoomsListView.View = System.Windows.Forms.View.Details;
            RoomsListView.FullRowSelect = true;
            int next = 0;
            if (Convert.ToInt32(roomsMsg.Substring(3, 4)) == 0)
            {
                msgLabel.Text = Constants.NO_ROOMS;
            }
            else
            {
                for (int i = 0; i < Convert.ToInt32(roomsMsg.Substring(3, 4)); i++)
                {
                    int roomNameLen = Convert.ToInt32(roomsMsg.Substring(11 + next, 2));
                    string roomsName = (roomsMsg.Substring(13+next, roomNameLen));
                    string roomId = roomsMsg.Substring(7 + next, 4);
                    string[] row = new string[] { roomsName, Convert.ToInt32(roomId).ToString() };
                    ListViewItem item = new ListViewItem(row);
                    RoomsListView.Items.Add(item);
                    RoomsListView.Refresh();
                    next = next + 6 + roomNameLen;
                }
            }
        }

        private void back_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            AdminRoomForm.id = RoomsListView.SelectedItems[0].SubItems[1].Text;
            MainScreen.MSG = Constants.JOIN_ROOM + RoomsListView.SelectedItems[0].SubItems[1].Text ;
            MainScreen.msgsHandler();
            this.Hide();
            Form userRoomCreate = new PlayerRoomForm();
            userRoomCreate.ShowDialog();
            this.Show();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            int next = 0;
            if (Convert.ToInt32(roomsMsg.Substring(3, 4)) == 0)
            {
                msgLabel.Text = Constants.NO_ROOMS;
            }
            else
            {
                RoomsListView.Items.Clear();
                 for (int i = 0; i < Convert.ToInt32(roomsMsg.Substring(3, 4)); i++)
                {
                    int roomNameLen = Convert.ToInt32(roomsMsg.Substring(11 + next, 2));
                    string roomsName = (roomsMsg.Substring(13+next, roomNameLen));
                    string roomId = roomsMsg.Substring(7 + next, 4);
                    string[] row = new string[] { roomsName, Convert.ToInt32(roomId).ToString() };
                    ListViewItem item = new ListViewItem(row);
                    RoomsListView.Items.Add(item);
                    RoomsListView.Refresh();
                    next = next + 6 + roomNameLen;
                }
            }
        }
    }
}
