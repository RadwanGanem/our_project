﻿namespace DanielAndRadwanProject_Client
{
    partial class AdminRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.RoomsNameLabel = new System.Windows.Forms.Label();
            this.closeRoomButton = new System.Windows.Forms.Button();
            this.startGameButton = new System.Windows.Forms.Button();
            this.UsersListView = new System.Windows.Forms.ListView();
            this.names = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.currentUsersAreLabel = new System.Windows.Forms.Label();
            this.MaxAmountOfUsersLabel = new System.Windows.Forms.Label();
            this.amountLabel = new System.Windows.Forms.Label();
            this.numberofQuestionsLabel = new System.Windows.Forms.Label();
            this.numberLabel = new System.Windows.Forms.Label();
            this.timePerQuestionLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(146, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 16;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // RoomsNameLabel
            // 
            this.RoomsNameLabel.AutoSize = true;
            this.RoomsNameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomsNameLabel.ForeColor = System.Drawing.Color.White;
            this.RoomsNameLabel.Location = new System.Drawing.Point(306, 139);
            this.RoomsNameLabel.Name = "RoomsNameLabel";
            this.RoomsNameLabel.Size = new System.Drawing.Size(171, 51);
            this.RoomsNameLabel.TabIndex = 20;
            this.RoomsNameLabel.Text = "[RoomsName]";
            this.RoomsNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // closeRoomButton
            // 
            this.closeRoomButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeRoomButton.ForeColor = System.Drawing.Color.Black;
            this.closeRoomButton.Location = new System.Drawing.Point(170, 509);
            this.closeRoomButton.Name = "closeRoomButton";
            this.closeRoomButton.Size = new System.Drawing.Size(204, 58);
            this.closeRoomButton.TabIndex = 21;
            this.closeRoomButton.Text = "Close Room";
            this.closeRoomButton.UseVisualStyleBackColor = true;
            this.closeRoomButton.Click += new System.EventHandler(this.closeRoomButton_Click);
            // 
            // startGameButton
            // 
            this.startGameButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startGameButton.ForeColor = System.Drawing.Color.Black;
            this.startGameButton.Location = new System.Drawing.Point(403, 509);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(204, 58);
            this.startGameButton.TabIndex = 22;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // UsersListView
            // 
            this.UsersListView.AllowColumnReorder = true;
            this.UsersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.names});
            this.UsersListView.Font = new System.Drawing.Font("Niagara Solid", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsersListView.Location = new System.Drawing.Point(184, 350);
            this.UsersListView.Name = "UsersListView";
            this.UsersListView.Size = new System.Drawing.Size(437, 135);
            this.UsersListView.TabIndex = 23;
            this.UsersListView.UseCompatibleStateImageBehavior = false;
            this.UsersListView.View = System.Windows.Forms.View.Details;
            // 
            // names
            // 
            this.names.Text = "Name";
            this.names.Width = 500;
            // 
            // currentUsersAreLabel
            // 
            this.currentUsersAreLabel.AutoSize = true;
            this.currentUsersAreLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentUsersAreLabel.ForeColor = System.Drawing.Color.White;
            this.currentUsersAreLabel.Location = new System.Drawing.Point(273, 278);
            this.currentUsersAreLabel.Name = "currentUsersAreLabel";
            this.currentUsersAreLabel.Size = new System.Drawing.Size(235, 51);
            this.currentUsersAreLabel.TabIndex = 24;
            this.currentUsersAreLabel.Text = "Current users are :";
            // 
            // MaxAmountOfUsersLabel
            // 
            this.MaxAmountOfUsersLabel.AutoSize = true;
            this.MaxAmountOfUsersLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaxAmountOfUsersLabel.ForeColor = System.Drawing.Color.White;
            this.MaxAmountOfUsersLabel.Location = new System.Drawing.Point(165, 190);
            this.MaxAmountOfUsersLabel.Name = "MaxAmountOfUsersLabel";
            this.MaxAmountOfUsersLabel.Size = new System.Drawing.Size(161, 29);
            this.MaxAmountOfUsersLabel.TabIndex = 25;
            this.MaxAmountOfUsersLabel.Text = "Max amount of Users : ";
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountLabel.ForeColor = System.Drawing.Color.White;
            this.amountLabel.Location = new System.Drawing.Point(319, 190);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(31, 29);
            this.amountLabel.TabIndex = 26;
            this.amountLabel.Text = "[0]";
            // 
            // numberofQuestionsLabel
            // 
            this.numberofQuestionsLabel.AutoSize = true;
            this.numberofQuestionsLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberofQuestionsLabel.ForeColor = System.Drawing.Color.White;
            this.numberofQuestionsLabel.Location = new System.Drawing.Point(423, 190);
            this.numberofQuestionsLabel.Name = "numberofQuestionsLabel";
            this.numberofQuestionsLabel.Size = new System.Drawing.Size(159, 29);
            this.numberofQuestionsLabel.TabIndex = 27;
            this.numberofQuestionsLabel.Text = "Number of Questions : ";
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberLabel.ForeColor = System.Drawing.Color.White;
            this.numberLabel.Location = new System.Drawing.Point(576, 190);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(31, 29);
            this.numberLabel.TabIndex = 28;
            this.numberLabel.Text = "[0]";
            // 
            // timePerQuestionLabel
            // 
            this.timePerQuestionLabel.AutoSize = true;
            this.timePerQuestionLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePerQuestionLabel.ForeColor = System.Drawing.Color.White;
            this.timePerQuestionLabel.Location = new System.Drawing.Point(310, 234);
            this.timePerQuestionLabel.Name = "timePerQuestionLabel";
            this.timePerQuestionLabel.Size = new System.Drawing.Size(146, 29);
            this.timePerQuestionLabel.TabIndex = 29;
            this.timePerQuestionLabel.Text = "Time Per Question : ";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.Color.White;
            this.timeLabel.Location = new System.Drawing.Point(446, 234);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(31, 29);
            this.timeLabel.TabIndex = 30;
            this.timeLabel.Text = "[0]";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(624, 516);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 31;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // AdminRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(789, 599);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.timePerQuestionLabel);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.numberofQuestionsLabel);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.MaxAmountOfUsersLabel);
            this.Controls.Add(this.currentUsersAreLabel);
            this.Controls.Add(this.UsersListView);
            this.Controls.Add(this.startGameButton);
            this.Controls.Add(this.closeRoomButton);
            this.Controls.Add(this.RoomsNameLabel);
            this.Controls.Add(this.Title);
            this.Name = "AdminRoomForm";
            this.Text = "RoomForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label RoomsNameLabel;
        private System.Windows.Forms.Button closeRoomButton;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.ListView UsersListView;
        private System.Windows.Forms.Label currentUsersAreLabel;
        private System.Windows.Forms.Label MaxAmountOfUsersLabel;
        private System.Windows.Forms.Label amountLabel;
        private System.Windows.Forms.Label numberofQuestionsLabel;
        private System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.Label timePerQuestionLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.ColumnHeader names;
    }
}