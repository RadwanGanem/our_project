﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;


namespace DanielAndRadwanProject_Client
{
    public partial class MainScreen : Form
    {
        public static string MSG { get; set; }
        public static string RecivedMSG { get; set; }
        public static TcpClient client;
        public static NetworkStream clientStream;
        public static byte[] key { get; set; }

        public MainScreen()
        {
            InitializeComponent();
            Thread c = new Thread(serverConnection);
            c.Start();
        }

        public static void sendMSG()
        {
            try
            {
                clientStream = client.GetStream();
                byte[] buffer = new ASCIIEncoding().GetBytes(MSG);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private static void reciveMSG()
        {
            try
            {
                byte[] bufferIn = new byte[4];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                RecivedMSG = new ASCIIEncoding().GetString(bufferIn);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        public static void msgsHandler()
        {
            sendMSG();
            reciveMSG();
        }

        private void serverConnection()
        {
            try
            {
                //start the connection
                client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(Constants.LOCAL_HOST), Constants.PORT);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                MSG = Constants.EXIT;
                Application.Exit();
            }

        }

        private void signIn_Click(object sender, EventArgs e)
        {
            try
            {

                if (userText.Text != Constants.EMPTY && passText.Text != Constants.EMPTY)
                {
                    //string pass = MD5encryption(passText.Text);
                    string pass = passText.Text;

                    erorrLabel.Text = Constants.EMPTY;

                    string uLen = (userText.Text.Length < 10) ? Constants.ZERO + userText.Text.Length.ToString() : userText.Text.Length.ToString();
                    string pLen = (pass.Length < 10) ? Constants.ZERO + pass.Length.ToString() : pass.Length.ToString();

                    MSG = Constants.SIGNIN + uLen + userText.Text + pLen + pass;
                    msgsHandler();
                    if (RecivedMSG == Constants.SUCCESS)
                    {
                        this.Hide();
                        Form LogIn = new LoggedInForm(userText.Text);
                        LogIn.ShowDialog();
                        this.Show();
                    }
                    else if (RecivedMSG == Constants.INVAILD_DETAILS)
                    {
                        erorrLabel.Text = Constants.INVAILD_FIELD_MSG;
                    }
                    else if (RecivedMSG == Constants.ALREADY_CONNECTED)
                    {
                        erorrLabel.Text = Constants.ALREADY_CONNECTED_MSG;
                    }
                }
                else
                {
                    erorrLabel.Text = Constants.EMPTY_FIELD_MSG;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void signUp_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                Form signUp = new SignUpForm();
                signUp.ShowDialog();
                this.Show();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                /*MSG = Constants.EXIT;
                msgsHandler();
                Application.Exit();*/
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            MSG = Constants.EXIT;
            msgsHandler();
            Application.Exit();
        }


        public static string MD5encryption(string input)

        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();
        }
    }
}
