﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class MyStatsForm : Form
    {
        public static string games { get; set; }
        public static string rightAnswers { get; set; }
        public static string WrongAnswers { get; set; }
        public static string time { get; set; }
        public MyStatsForm()
        {
            InitializeComponent();
            usernameLabel.Text = LoggedInForm.userConnected;
            gamesLabel.Text = games;
            rightLabel.Text = rightAnswers;
            wrongLabel.Text = WrongAnswers;
            timeLabel.Text = time;
        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
