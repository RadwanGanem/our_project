﻿namespace DanielAndRadwanProject_Client
{
    partial class PlayerRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.timePerQuestionLabel = new System.Windows.Forms.Label();
            this.numberLabel = new System.Windows.Forms.Label();
            this.numberofQuestionsLabel = new System.Windows.Forms.Label();
            this.currentUsersAreLabel = new System.Windows.Forms.Label();
            this.leaveRoomButton = new System.Windows.Forms.Button();
            this.RoomsNameLabel = new System.Windows.Forms.Label();
            this.UsersListView = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(165, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 17;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(611, 451);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 39;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.Color.White;
            this.timeLabel.Location = new System.Drawing.Point(340, 188);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(31, 29);
            this.timeLabel.TabIndex = 38;
            this.timeLabel.Text = "[0]";
            // 
            // timePerQuestionLabel
            // 
            this.timePerQuestionLabel.AutoSize = true;
            this.timePerQuestionLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePerQuestionLabel.ForeColor = System.Drawing.Color.White;
            this.timePerQuestionLabel.Location = new System.Drawing.Point(199, 188);
            this.timePerQuestionLabel.Name = "timePerQuestionLabel";
            this.timePerQuestionLabel.Size = new System.Drawing.Size(146, 29);
            this.timePerQuestionLabel.TabIndex = 37;
            this.timePerQuestionLabel.Text = "Time Per Question : ";
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberLabel.ForeColor = System.Drawing.Color.White;
            this.numberLabel.Location = new System.Drawing.Point(610, 188);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(31, 29);
            this.numberLabel.TabIndex = 36;
            this.numberLabel.Text = "[0]";
            // 
            // numberofQuestionsLabel
            // 
            this.numberofQuestionsLabel.AutoSize = true;
            this.numberofQuestionsLabel.Font = new System.Drawing.Font("Niagara Engraved", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberofQuestionsLabel.ForeColor = System.Drawing.Color.White;
            this.numberofQuestionsLabel.Location = new System.Drawing.Point(445, 188);
            this.numberofQuestionsLabel.Name = "numberofQuestionsLabel";
            this.numberofQuestionsLabel.Size = new System.Drawing.Size(159, 29);
            this.numberofQuestionsLabel.TabIndex = 35;
            this.numberofQuestionsLabel.Text = "Number of Questions : ";
            // 
            // currentUsersAreLabel
            // 
            this.currentUsersAreLabel.AutoSize = true;
            this.currentUsersAreLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentUsersAreLabel.ForeColor = System.Drawing.Color.White;
            this.currentUsersAreLabel.Location = new System.Drawing.Point(284, 238);
            this.currentUsersAreLabel.Name = "currentUsersAreLabel";
            this.currentUsersAreLabel.Size = new System.Drawing.Size(235, 51);
            this.currentUsersAreLabel.TabIndex = 34;
            this.currentUsersAreLabel.Text = "Current users are :";
            // 
            // leaveRoomButton
            // 
            this.leaveRoomButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaveRoomButton.ForeColor = System.Drawing.Color.Black;
            this.leaveRoomButton.Location = new System.Drawing.Point(305, 444);
            this.leaveRoomButton.Name = "leaveRoomButton";
            this.leaveRoomButton.Size = new System.Drawing.Size(204, 58);
            this.leaveRoomButton.TabIndex = 33;
            this.leaveRoomButton.Text = "Leave Room";
            this.leaveRoomButton.UseVisualStyleBackColor = true;
            this.leaveRoomButton.Click += new System.EventHandler(this.leaveRoomButton_Click);
            // 
            // RoomsNameLabel
            // 
            this.RoomsNameLabel.AutoSize = true;
            this.RoomsNameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomsNameLabel.ForeColor = System.Drawing.Color.White;
            this.RoomsNameLabel.Location = new System.Drawing.Point(321, 137);
            this.RoomsNameLabel.Name = "RoomsNameLabel";
            this.RoomsNameLabel.Size = new System.Drawing.Size(171, 51);
            this.RoomsNameLabel.TabIndex = 32;
            this.RoomsNameLabel.Text = "[RoomsName]";
            this.RoomsNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UsersListView
            // 
            this.UsersListView.Location = new System.Drawing.Point(204, 292);
            this.UsersListView.Name = "UsersListView";
            this.UsersListView.Size = new System.Drawing.Size(437, 135);
            this.UsersListView.TabIndex = 40;
            this.UsersListView.UseCompatibleStateImageBehavior = false;
            // 
            // PlayerRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(855, 563);
            this.Controls.Add(this.UsersListView);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.timePerQuestionLabel);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.numberofQuestionsLabel);
            this.Controls.Add(this.currentUsersAreLabel);
            this.Controls.Add(this.leaveRoomButton);
            this.Controls.Add(this.RoomsNameLabel);
            this.Controls.Add(this.Title);
            this.ForeColor = System.Drawing.Color.SeaShell;
            this.Name = "PlayerRoomForm";
            this.Text = "PlayerRoomForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label timePerQuestionLabel;
        private System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.Label numberofQuestionsLabel;
        private System.Windows.Forms.Label currentUsersAreLabel;
        private System.Windows.Forms.Button leaveRoomButton;
        private System.Windows.Forms.Label RoomsNameLabel;
        private System.Windows.Forms.ListView UsersListView;
    }
}