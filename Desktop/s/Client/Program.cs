﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    static class Constants // defines
    {
        public const string LOCAL_HOST = "127.0.0.1", 
            ZERO = "0" , 
            RECIVED_ALL_USERS = "118",
            CHECK_ANSWER = "120" , 
            SIGNIN = "200" , 
            SIGNUP = "203" , 
            SIGNOUT = "201" , 
            ROOM_REQUEST = "205" ,
            JOIN_ROOM = "209", 
            EXIT = "299", 
            CREATE_ROOM = "213",
            CLOSE_ROOM = "215", 
            START_GAME = "217", 
            HELLO = "Hello " ,
            LEAVE_ROOM = "211",
            ANSWER_SEND = "219",
            GAME_LEFT = "222",
            BEST_SCORES = "223",
            MY_STATS = "225",
            EMPTY = "", 
            SUCCESS = "1020" , 
            SIGNUP_SUCCESS = "1040", 
            INVAILD_DETAILS = "1021", 
            ALREADY_CONNECTED = "1022" , 
            ILEGAL_PASS = "1041",
            USERNAME_EXISTS = "1042",
            USERNAME_ILLEGAL = "1043",
            OTHER = "1044",
            CREATE_ROOM_SUCCESS = "1140",
            CREATE_ROOM_FAILED = "1141",
            EMPTY_FIELD_MSG = "One or more fields are empty!!!" , 
            INVAILD_FIELD_MSG = "Invaild username or password!!!" , 
            ALREADY_CONNECTED_MSG = "The user already connected" , 
            ILEGAL_PASS_MSG = "Password is ilegal" , 
            USERNAME_EXISTS_MSG = "Username already exists" , 
            USERNAME_ILLEGAL_MSG = "Username is illegal" , 
            OTHER_MSG = "Try again" , 
            CREATE_ROOM_FAILED_MSG = "Failed to create the room" , 
            NO_ROOMS = "There is no rooms";
        public const int PORT = 8820; 

    }
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainScreen());
        }
    }
}
