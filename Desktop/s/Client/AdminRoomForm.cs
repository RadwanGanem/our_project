﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class AdminRoomForm : Form
    {
        public static string RoomName { get; set; } //the name of the room
        public static string user { get; set; } //number of users
        public static string amount { get; set; } //amount of questions
        public static string time { get; set; } // time per question
        public static string id { get; set; } //id of the room 
        public static string usersMsg { get; set; } //recived msg from the room of the users in the room

        public AdminRoomForm()
        {
            InitializeComponent();
            usernameLabel.Text = LoggedInForm.userConnected;
            UsersListView.View = View.Details;
            UsersListView.FullRowSelect = true;
            RoomsNameLabel.Text = RoomName;
            string[] row = { LoggedInForm.userConnected };
            ListViewItem item = new ListViewItem(row);
            UsersListView.Items.Add(item);
            //UsersListView.Refresh();
         
            amountLabel.Text = user;
            numberLabel.Text = amount;
            timeLabel.Text = time; 
        }

        public static void reciveQuestionAndAnswers()
        {
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 1000);
            string recivedMsg = new ASCIIEncoding().GetString(bufferIn);
            int IndexToMove = (recivedMsg.Substring(4, 3) == Constants.RECIVED_ALL_USERS) ? 7 : 3 ;
            int QLen = Convert.ToInt32(recivedMsg.Substring(IndexToMove, 3));
            IndexToMove = IndexToMove + 3;
            QuestionForm.question = recivedMsg.Substring(IndexToMove, QLen);
            IndexToMove = IndexToMove + QLen;
            int A1Len = Convert.ToInt32(recivedMsg.Substring(IndexToMove, 3));
            IndexToMove = IndexToMove + 3; 
            QuestionForm.answer01 = recivedMsg.Substring(IndexToMove, A1Len);
            IndexToMove = IndexToMove + A1Len;
            int A2Len = Convert.ToInt32(recivedMsg.Substring(IndexToMove, 3));
            IndexToMove = IndexToMove + 3 ;
            QuestionForm.answer02 = recivedMsg.Substring(IndexToMove, A2Len);
            IndexToMove = IndexToMove + A2Len;
            int A3Len = Convert.ToInt32(recivedMsg.Substring(IndexToMove, 3));
            IndexToMove = IndexToMove + 3;
            QuestionForm.answer03 = recivedMsg.Substring(IndexToMove, A3Len);
            IndexToMove = IndexToMove + A3Len;
            int A4Len = Convert.ToInt32(recivedMsg.Substring(IndexToMove, 3));
            IndexToMove = IndexToMove + 3 ;
            QuestionForm.answer04 = recivedMsg.Substring(IndexToMove, A4Len);
        }

        private void closeRoomButton_Click(object sender, EventArgs e)
        {
            MainScreen.MSG = Constants.CLOSE_ROOM;
            MainScreen.msgsHandler();
            this.Close();
        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MainScreen.MSG = Constants.START_GAME;
            MainScreen.sendMSG();
            reciveQuestionAndAnswers();
            QuestionForm.time = Convert.ToInt32(time);
            QuestionForm.answered = 0;
            Form questions = new QuestionForm();
            questions.ShowDialog();
            showResults();
            MainScreen.MSG = Constants.GAME_LEFT;
            MainScreen.msgsHandler();
            this.Show();
        }

        public static void updateUsers()
        {
            MainScreen.sendMSG();
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 1000);
            AdminRoomForm.usersMsg = new ASCIIEncoding().GetString(bufferIn);
            ListView UsersListView = new ListView();
            UsersListView.Clear();
            updateUsers();
            int next = 0;
            for (int i = 0; i < Convert.ToInt32(AdminRoomForm.usersMsg.Substring(3, 1)); i = +next)
            {
                int usernameLen = Convert.ToInt32(usersMsg.Substring(5, 2));
                string username = (usersMsg.Substring(13, usernameLen));
                string[] row = { username };
                ListViewItem item = new ListViewItem(row);
                UsersListView.Items.Add(item);
                UsersListView.Refresh();
                next = next + 3 + usernameLen;
                UsersListView.Refresh();
            }
           
        }

        public static void showResults()
        {
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn, 0, 1000);
            string ans = new ASCIIEncoding().GetString(bufferIn);
            string toPrint = "";
            int place = 4;
            int unSize;
            for (int i = Convert.ToInt32(ans.Substring(3, 1)); i > 0; i--)
            {
                unSize = Convert.ToInt32(ans.Substring(place, 2));
                place = place + 2;
                toPrint = toPrint + (ans.Substring(place, 2)) + ":";
                place = place + unSize;
                toPrint = toPrint + (ans.Substring(place, 2));
            }
            MessageBox.Show(toPrint);
        }
    }
}
