﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class QuestionForm : Form
    {
        public static string question { get; set; }
        public static string answer01 { get; set; }
        public static string answer02 { get; set; }
        public static string answer03 { get; set; }
        public static string answer04 { get; set; }
        public static int time { get; set; }
        public static int answered { get; set; }
        public static int end { get; set; }
        Timer timer = new Timer();
        public QuestionForm()
        {
            InitializeComponent();
            questionLabel.Text = question;
            answer1.Text = answer01;
            answer2.Text = answer02;
            answer3.Text = answer03;
            answer4.Text = answer04;
            timeLabel.Text = time.ToString();
            timer.Interval = (1000);
            timer.Tick += new EventHandler(timer_Tick); // Everytime timer ticks, timer_Tick will be called
            timer.Enabled = true;                       // Enable the timer
            timer.Start();

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            time = time - 1;
            timeLabel.Text = time.ToString();
            if (answered != 0)
            {
                this.Controls["answer" + (answered).ToString()].BackColor = (recivesFromServer()) ? Color.Green : Color.Red;
                for (int i = 0; i < 4; i++)
                {
                    if (i != answered)
                    {
                        this.Controls["answer" + (i + 1).ToString()].BackColor = Color.Red;
                    }
                }
                if (end == Convert.ToInt32(AdminRoomForm.amount))
                {
                    recivesFromServer();
                }
                restart();
            }
            else if (time == 0 && answered == 0)
            {
                answered = 5;
                recivesFromServer();
                for (int i = 0; i < 4; i++)
                {
                    this.Controls["answer" + (i + 1).ToString()].BackColor = Color.Red;
                }
                if (end == Convert.ToInt32(AdminRoomForm.amount))
                {
                    recivesFromServer();
                }
                restart();
            }

        }

        private void answer1_Click(object sender, EventArgs e)
        {
            answered = 1;
        }

        private void answer2_Click(object sender, EventArgs e)
        {
            answered = 2;
        }

        private void answer3_Click(object sender, EventArgs e)
        {
            answered = 3;
        }

        private void answer4_Click(object sender, EventArgs e)
        {
            answered = 4;
        }

        bool recivesFromServer()
        {
            if (end == Convert.ToInt32(AdminRoomForm.amount))
            {
                this.Close(); //Form is'nt closes here
            }
            string timeToSend = ((Convert.ToInt32(AdminRoomForm.time) - time) < 10) ? Constants.ZERO + (Convert.ToInt32(AdminRoomForm.time) - time).ToString() : (Convert.ToInt32(AdminRoomForm.time) - time).ToString();
            MainScreen.MSG = Constants.ANSWER_SEND + QuestionForm.answered.ToString() + timeToSend;//(time - time).ToString();
            MainScreen.sendMSG();
            byte[] bufferIn = new byte[1000];
            int bytesRead = MainScreen.clientStream.Read(bufferIn,0,1000);
            string ans = new ASCIIEncoding().GetString(bufferIn);
            int rightOrNot = Convert.ToInt32(ans.Substring(3, 1));
            bool toRet = (rightOrNot == 1) ? true : false;
            end = end + 1;
            return toRet;
        }

        void restart()
        {
            time = Convert.ToInt32(AdminRoomForm.time);
            answered = 0;
            AdminRoomForm.reciveQuestionAndAnswers();
            timer.Enabled = false;
            for (int i = 0; i < 4; i++)
            {
                this.Controls["answer" + (i + 1).ToString()].BackColor = Color.LightGray;
            }
            questionLabel.Text = question;
            answer1.Text = answer01;
            answer2.Text = answer02;
            answer3.Text = answer03;
            answer4.Text = answer04;
            timeLabel.Text = time.ToString();
            timer.Interval = (1000);
            timer.Tick += new EventHandler(timer_Tick); // Everytime timer ticks, timer_Tick will be called
            timer.Enabled = true;                       // Enable the timer
            timer.Start();
        }

    }
}
