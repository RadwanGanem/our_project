﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class PlayerRoomForm : Form
    {
        public PlayerRoomForm()
        {
            InitializeComponent();
            AdminRoomForm.updateUsers();
            RoomsNameLabel.Text = AdminRoomForm.RoomName;
            numberLabel.Text = AdminRoomForm.amount;
            timeLabel.Text = AdminRoomForm.time;
            UsersListView.FullRowSelect = true;
            int next = 0;
            for (int i = 0; i < Convert.ToInt32(AdminRoomForm.usersMsg.Substring(3, 1)); i = +next)
            {
                int usernameLen = Convert.ToInt32(AdminRoomForm.usersMsg.Substring(5, 2));
                string username = (AdminRoomForm.usersMsg.Substring(13, usernameLen));
                string[] row = { username };
                ListViewItem item = new ListViewItem(row);
                UsersListView.Items.Add(item);
                UsersListView.Refresh();
                next = next + 3 + usernameLen;
                UsersListView.Refresh();
            }
        }

        private void leaveRoomButton_Click(object sender, EventArgs e)
        {
            MainScreen.MSG = Constants.LEAVE_ROOM;
            MainScreen.msgsHandler();
            this.Close();
        }
    }
}
