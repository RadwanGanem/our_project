﻿namespace DanielAndRadwanProject_Client
{
    partial class MyStatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.gamesPlayedLabel = new System.Windows.Forms.Label();
            this.gamesLabel = new System.Windows.Forms.Label();
            this.rAnswersLabel = new System.Windows.Forms.Label();
            this.wAnswersNum = new System.Windows.Forms.Label();
            this.averageTimeForAnswerLabel = new System.Windows.Forms.Label();
            this.rightLabel = new System.Windows.Forms.Label();
            this.wrongLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(142, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 18;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // gamesPlayedLabel
            // 
            this.gamesPlayedLabel.AutoSize = true;
            this.gamesPlayedLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gamesPlayedLabel.ForeColor = System.Drawing.Color.White;
            this.gamesPlayedLabel.Location = new System.Drawing.Point(87, 180);
            this.gamesPlayedLabel.Name = "gamesPlayedLabel";
            this.gamesPlayedLabel.Size = new System.Drawing.Size(191, 51);
            this.gamesPlayedLabel.TabIndex = 26;
            this.gamesPlayedLabel.Text = "Games Played : ";
            // 
            // gamesLabel
            // 
            this.gamesLabel.AutoSize = true;
            this.gamesLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gamesLabel.ForeColor = System.Drawing.Color.White;
            this.gamesLabel.Location = new System.Drawing.Point(263, 180);
            this.gamesLabel.Name = "gamesLabel";
            this.gamesLabel.Size = new System.Drawing.Size(51, 51);
            this.gamesLabel.TabIndex = 27;
            this.gamesLabel.Text = "[0]";
            // 
            // rAnswersLabel
            // 
            this.rAnswersLabel.AutoSize = true;
            this.rAnswersLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rAnswersLabel.ForeColor = System.Drawing.Color.White;
            this.rAnswersLabel.Location = new System.Drawing.Point(376, 180);
            this.rAnswersLabel.Name = "rAnswersLabel";
            this.rAnswersLabel.Size = new System.Drawing.Size(201, 51);
            this.rAnswersLabel.TabIndex = 28;
            this.rAnswersLabel.Text = "Right Answers : ";
            // 
            // wAnswersNum
            // 
            this.wAnswersNum.AutoSize = true;
            this.wAnswersNum.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wAnswersNum.ForeColor = System.Drawing.Color.White;
            this.wAnswersNum.Location = new System.Drawing.Point(87, 255);
            this.wAnswersNum.Name = "wAnswersNum";
            this.wAnswersNum.Size = new System.Drawing.Size(215, 51);
            this.wAnswersNum.TabIndex = 29;
            this.wAnswersNum.Text = "Wrong Answers : ";
            // 
            // averageTimeForAnswerLabel
            // 
            this.averageTimeForAnswerLabel.AutoSize = true;
            this.averageTimeForAnswerLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageTimeForAnswerLabel.ForeColor = System.Drawing.Color.White;
            this.averageTimeForAnswerLabel.Location = new System.Drawing.Point(376, 255);
            this.averageTimeForAnswerLabel.Name = "averageTimeForAnswerLabel";
            this.averageTimeForAnswerLabel.Size = new System.Drawing.Size(332, 51);
            this.averageTimeForAnswerLabel.TabIndex = 30;
            this.averageTimeForAnswerLabel.Text = "Average Time For Answer : ";
            // 
            // rightLabel
            // 
            this.rightLabel.AutoSize = true;
            this.rightLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightLabel.ForeColor = System.Drawing.Color.White;
            this.rightLabel.Location = new System.Drawing.Point(570, 180);
            this.rightLabel.Name = "rightLabel";
            this.rightLabel.Size = new System.Drawing.Size(51, 51);
            this.rightLabel.TabIndex = 31;
            this.rightLabel.Text = "[0]";
            // 
            // wrongLabel
            // 
            this.wrongLabel.AutoSize = true;
            this.wrongLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wrongLabel.ForeColor = System.Drawing.Color.White;
            this.wrongLabel.Location = new System.Drawing.Point(291, 255);
            this.wrongLabel.Name = "wrongLabel";
            this.wrongLabel.Size = new System.Drawing.Size(51, 51);
            this.wrongLabel.TabIndex = 32;
            this.wrongLabel.Text = "[0]";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.Color.White;
            this.timeLabel.Location = new System.Drawing.Point(690, 255);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(51, 51);
            this.timeLabel.TabIndex = 33;
            this.timeLabel.Text = "[0]";
            // 
            // back
            // 
            this.back.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.Black;
            this.back.Location = new System.Drawing.Point(300, 339);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(204, 58);
            this.back.TabIndex = 34;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(635, 390);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 35;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // MyStatsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.back);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.wrongLabel);
            this.Controls.Add(this.rightLabel);
            this.Controls.Add(this.averageTimeForAnswerLabel);
            this.Controls.Add(this.wAnswersNum);
            this.Controls.Add(this.rAnswersLabel);
            this.Controls.Add(this.gamesLabel);
            this.Controls.Add(this.gamesPlayedLabel);
            this.Controls.Add(this.Title);
            this.Name = "MyStatsForm";
            this.Text = "MyStatsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label gamesPlayedLabel;
        private System.Windows.Forms.Label gamesLabel;
        private System.Windows.Forms.Label rAnswersLabel;
        private System.Windows.Forms.Label wAnswersNum;
        private System.Windows.Forms.Label averageTimeForAnswerLabel;
        private System.Windows.Forms.Label rightLabel;
        private System.Windows.Forms.Label wrongLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label usernameLabel;
    }
}