﻿namespace DanielAndRadwanProject_Client
{
    partial class CreateRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SendButton = new System.Windows.Forms.Button();
            this.numberOfPlayersText = new System.Windows.Forms.TextBox();
            this.roomNameText = new System.Windows.Forms.TextBox();
            this.numberOfPlayersLabel = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.timeForQuestionText = new System.Windows.Forms.TextBox();
            this.numberOfQuestionsText = new System.Windows.Forms.TextBox();
            this.timeForQuestionLabel = new System.Windows.Forms.Label();
            this.numberOfQuestionsLabel = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.erorrLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SendButton
            // 
            this.SendButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendButton.ForeColor = System.Drawing.Color.Black;
            this.SendButton.Location = new System.Drawing.Point(171, 316);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(204, 58);
            this.SendButton.TabIndex = 1;
            this.SendButton.Text = "Send";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // numberOfPlayersText
            // 
            this.numberOfPlayersText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersText.Location = new System.Drawing.Point(389, 179);
            this.numberOfPlayersText.MaxLength = 1;
            this.numberOfPlayersText.Name = "numberOfPlayersText";
            this.numberOfPlayersText.Size = new System.Drawing.Size(284, 26);
            this.numberOfPlayersText.TabIndex = 9;
            // 
            // roomNameText
            // 
            this.roomNameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roomNameText.Location = new System.Drawing.Point(389, 147);
            this.roomNameText.Name = "roomNameText";
            this.roomNameText.Size = new System.Drawing.Size(284, 26);
            this.roomNameText.TabIndex = 8;
            // 
            // numberOfPlayersLabel
            // 
            this.numberOfPlayersLabel.AutoSize = true;
            this.numberOfPlayersLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersLabel.ForeColor = System.Drawing.Color.White;
            this.numberOfPlayersLabel.Location = new System.Drawing.Point(167, 180);
            this.numberOfPlayersLabel.Name = "numberOfPlayersLabel";
            this.numberOfPlayersLabel.Size = new System.Drawing.Size(162, 23);
            this.numberOfPlayersLabel.TabIndex = 7;
            this.numberOfPlayersLabel.Text = "Number Of Players :";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roomNameLabel.ForeColor = System.Drawing.Color.White;
            this.roomNameLabel.Location = new System.Drawing.Point(167, 147);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(103, 23);
            this.roomNameLabel.TabIndex = 6;
            this.roomNameLabel.Text = "Room Name :";
            // 
            // timeForQuestionText
            // 
            this.timeForQuestionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeForQuestionText.Location = new System.Drawing.Point(389, 243);
            this.timeForQuestionText.MaxLength = 2;
            this.timeForQuestionText.Name = "timeForQuestionText";
            this.timeForQuestionText.Size = new System.Drawing.Size(284, 26);
            this.timeForQuestionText.TabIndex = 13;
            // 
            // numberOfQuestionsText
            // 
            this.numberOfQuestionsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfQuestionsText.Location = new System.Drawing.Point(389, 211);
            this.numberOfQuestionsText.MaxLength = 2;
            this.numberOfQuestionsText.Name = "numberOfQuestionsText";
            this.numberOfQuestionsText.Size = new System.Drawing.Size(284, 26);
            this.numberOfQuestionsText.TabIndex = 12;
            // 
            // timeForQuestionLabel
            // 
            this.timeForQuestionLabel.AutoSize = true;
            this.timeForQuestionLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeForQuestionLabel.ForeColor = System.Drawing.Color.White;
            this.timeForQuestionLabel.Location = new System.Drawing.Point(167, 246);
            this.timeForQuestionLabel.Name = "timeForQuestionLabel";
            this.timeForQuestionLabel.Size = new System.Drawing.Size(156, 23);
            this.timeForQuestionLabel.TabIndex = 11;
            this.timeForQuestionLabel.Text = "Time For Question :";
            // 
            // numberOfQuestionsLabel
            // 
            this.numberOfQuestionsLabel.AutoSize = true;
            this.numberOfQuestionsLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfQuestionsLabel.ForeColor = System.Drawing.Color.White;
            this.numberOfQuestionsLabel.Location = new System.Drawing.Point(167, 214);
            this.numberOfQuestionsLabel.Name = "numberOfQuestionsLabel";
            this.numberOfQuestionsLabel.Size = new System.Drawing.Size(181, 23);
            this.numberOfQuestionsLabel.TabIndex = 10;
            this.numberOfQuestionsLabel.Text = "Number Of Questions :";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(161, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 18;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(635, 390);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 26;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // back
            // 
            this.back.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.Black;
            this.back.Location = new System.Drawing.Point(469, 316);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(204, 58);
            this.back.TabIndex = 27;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // erorrLabel
            // 
            this.erorrLabel.AutoSize = true;
            this.erorrLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.erorrLabel.ForeColor = System.Drawing.Color.White;
            this.erorrLabel.Location = new System.Drawing.Point(348, 281);
            this.erorrLabel.Name = "erorrLabel";
            this.erorrLabel.Size = new System.Drawing.Size(0, 23);
            this.erorrLabel.TabIndex = 28;
            this.erorrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CreateRoomForm
            // 
            this.AccessibleName = "usernameText";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.erorrLabel);
            this.Controls.Add(this.back);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.timeForQuestionText);
            this.Controls.Add(this.numberOfQuestionsText);
            this.Controls.Add(this.timeForQuestionLabel);
            this.Controls.Add(this.numberOfQuestionsLabel);
            this.Controls.Add(this.numberOfPlayersText);
            this.Controls.Add(this.roomNameText);
            this.Controls.Add(this.numberOfPlayersLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.SendButton);
            this.Name = "CreateRoomForm";
            this.Text = "CreateRoomForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.TextBox numberOfPlayersText;
        private System.Windows.Forms.TextBox roomNameText;
        private System.Windows.Forms.Label numberOfPlayersLabel;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.TextBox timeForQuestionText;
        private System.Windows.Forms.TextBox numberOfQuestionsText;
        private System.Windows.Forms.Label timeForQuestionLabel;
        private System.Windows.Forms.Label numberOfQuestionsLabel;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label erorrLabel;
    }
}