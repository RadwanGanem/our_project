﻿namespace DanielAndRadwanProject_Client
{
    partial class LoggedInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.bestScoresButton = new System.Windows.Forms.Button();
            this.stats = new System.Windows.Forms.Button();
            this.createRoom = new System.Windows.Forms.Button();
            this.joinRoom = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameHelloLabel = new System.Windows.Forms.Label();
            this.logOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(155, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 23;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // quit
            // 
            this.quit.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.Black;
            this.quit.Location = new System.Drawing.Point(301, 423);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(204, 58);
            this.quit.TabIndex = 22;
            this.quit.Text = "Quit";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // bestScoresButton
            // 
            this.bestScoresButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bestScoresButton.ForeColor = System.Drawing.Color.Black;
            this.bestScoresButton.Location = new System.Drawing.Point(433, 348);
            this.bestScoresButton.Name = "bestScoresButton";
            this.bestScoresButton.Size = new System.Drawing.Size(204, 58);
            this.bestScoresButton.TabIndex = 21;
            this.bestScoresButton.Text = "Rest score";
            this.bestScoresButton.UseVisualStyleBackColor = true;
            this.bestScoresButton.Click += new System.EventHandler(this.bestScoresButton_Click);
            // 
            // stats
            // 
            this.stats.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stats.ForeColor = System.Drawing.Color.Black;
            this.stats.Location = new System.Drawing.Point(165, 348);
            this.stats.Name = "stats";
            this.stats.Size = new System.Drawing.Size(204, 58);
            this.stats.TabIndex = 20;
            this.stats.Text = "My stats";
            this.stats.UseVisualStyleBackColor = true;
            this.stats.Click += new System.EventHandler(this.stats_Click);
            // 
            // createRoom
            // 
            this.createRoom.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createRoom.ForeColor = System.Drawing.Color.Black;
            this.createRoom.Location = new System.Drawing.Point(433, 271);
            this.createRoom.Name = "createRoom";
            this.createRoom.Size = new System.Drawing.Size(204, 58);
            this.createRoom.TabIndex = 19;
            this.createRoom.Text = "Create room";
            this.createRoom.UseVisualStyleBackColor = true;
            this.createRoom.Click += new System.EventHandler(this.createRoom_Click);
            // 
            // joinRoom
            // 
            this.joinRoom.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinRoom.ForeColor = System.Drawing.Color.Black;
            this.joinRoom.Location = new System.Drawing.Point(165, 271);
            this.joinRoom.Name = "joinRoom";
            this.joinRoom.Size = new System.Drawing.Size(204, 58);
            this.joinRoom.TabIndex = 18;
            this.joinRoom.Text = "Join room";
            this.joinRoom.UseVisualStyleBackColor = true;
            this.joinRoom.Click += new System.EventHandler(this.joinRoom_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(624, 528);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 24;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // usernameHelloLabel
            // 
            this.usernameHelloLabel.AutoSize = true;
            this.usernameHelloLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameHelloLabel.ForeColor = System.Drawing.Color.White;
            this.usernameHelloLabel.Location = new System.Drawing.Point(195, 202);
            this.usernameHelloLabel.Name = "usernameHelloLabel";
            this.usernameHelloLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameHelloLabel.TabIndex = 25;
            this.usernameHelloLabel.Text = "[USERNAME]";
            // 
            // logOutButton
            // 
            this.logOutButton.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOutButton.ForeColor = System.Drawing.Color.Black;
            this.logOutButton.Location = new System.Drawing.Point(433, 195);
            this.logOutButton.Name = "logOutButton";
            this.logOutButton.Size = new System.Drawing.Size(204, 58);
            this.logOutButton.TabIndex = 26;
            this.logOutButton.Text = "Log Out";
            this.logOutButton.UseVisualStyleBackColor = true;
            this.logOutButton.Click += new System.EventHandler(this.logOutButton_Click);
            // 
            // LoggedInForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(789, 599);
            this.Controls.Add(this.logOutButton);
            this.Controls.Add(this.usernameHelloLabel);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.bestScoresButton);
            this.Controls.Add(this.stats);
            this.Controls.Add(this.createRoom);
            this.Controls.Add(this.joinRoom);
            this.Name = "LoggedInForm";
            this.Text = "5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Button bestScoresButton;
        private System.Windows.Forms.Button stats;
        private System.Windows.Forms.Button createRoom;
        private System.Windows.Forms.Button joinRoom;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label usernameHelloLabel;
        private System.Windows.Forms.Button logOutButton;
    }
}