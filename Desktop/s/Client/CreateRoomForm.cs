﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class CreateRoomForm : Form
    {
        public CreateRoomForm()
        {
            InitializeComponent();
            usernameLabel.Text = LoggedInForm.userConnected;
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (roomNameText.Text != Constants.EMPTY && numberOfPlayersText.Text != Constants.EMPTY && numberOfQuestionsText.Text != Constants.EMPTY && timeForQuestionText.Text != Constants.EMPTY)
                {
                    string nLen = (roomNameText.Text.Length < 10) ? Constants.ZERO + roomNameText.Text.Length.ToString() : roomNameText.Text.Length.ToString();
                    string q = (Int32.Parse(numberOfQuestionsText.Text) < 10) ? Constants.ZERO + numberOfQuestionsText.Text : numberOfQuestionsText.Text;
                    string t = (Int32.Parse(timeForQuestionText.Text) < 10 ) ? Constants.ZERO + timeForQuestionText.Text : timeForQuestionText.Text;
                    erorrLabel.Text = Constants.EMPTY;
                    MainScreen.MSG = Constants.CREATE_ROOM + nLen + roomNameText.Text + numberOfPlayersText.Text + q + t ;
                    MainScreen.msgsHandler();
                    if (MainScreen.RecivedMSG == Constants.CREATE_ROOM_SUCCESS)
                    {
                        this.Hide();
                        AdminRoomForm.RoomName = roomNameText.Text;
                        AdminRoomForm.user = numberOfPlayersText.Text;
                        AdminRoomForm.amount = numberOfQuestionsText.Text;
                        AdminRoomForm.time = timeForQuestionText.Text;
                        Form adminRoomCreate = new AdminRoomForm();
                        adminRoomCreate.ShowDialog();
                        this.Show();
                    }
                    else if(MainScreen.RecivedMSG == Constants.CREATE_ROOM_FAILED)
                    {
                        erorrLabel.Text = Constants.CREATE_ROOM_FAILED_MSG;
                    }
                }
                else
                {
                    erorrLabel.Text = Constants.EMPTY_FIELD_MSG;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

        private void back_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
