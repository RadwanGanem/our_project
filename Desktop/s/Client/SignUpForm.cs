﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace DanielAndRadwanProject_Client
{
    public partial class SignUpForm : Form
    {
        public SignUpForm()
        {
            InitializeComponent();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (userText.Text != null && passText.Text != null && emailText != null)
                {
                    string password = passText.Text;
                    erorrLabel.Text = Constants.EMPTY;
                    string uLen = userText.Text.Length.ToString();
                    string pLen = password.Length.ToString();
                    string eLen = emailText.Text.Length.ToString();
                    if (userText.Text.Length < 10)
                    {
                        uLen = "0" + userText.Text.Length.ToString();
                    }
                    if (password.Length < 10)
                    {
                        pLen = "0" + password.Length.ToString();
                    }
                    if (emailText.Text.Length < 10)
                    {
                        eLen = "0" + emailText.Text.Length.ToString();
                    }
                    MainScreen.MSG = Constants.SIGNUP + uLen + userText.Text + pLen + password + eLen + emailText.Text;
                    MainScreen.msgsHandler();
                    if (MainScreen.RecivedMSG == Constants.SIGNUP_SUCCESS)
                    {
                        this.Hide();
                    }
                    else if(MainScreen.RecivedMSG == Constants.ILEGAL_PASS)
                    {
                        erorrLabel.Text = Constants.ILEGAL_PASS_MSG;
                    }
                    else if (MainScreen.RecivedMSG == Constants.USERNAME_EXISTS)
                    {
                        erorrLabel.Text = Constants.USERNAME_EXISTS_MSG;
                    }
                    else if (MainScreen.RecivedMSG == Constants.USERNAME_ILLEGAL)
                    {
                        erorrLabel.Text = Constants.USERNAME_ILLEGAL_MSG;

                    }
                    else if (MainScreen.RecivedMSG == Constants.OTHER)
                    {
                        erorrLabel.Text = Constants.OTHER_MSG;
                    }
                }
                else
                {
                    erorrLabel.Text = Constants.EMPTY_FIELD_MSG;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void back_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
