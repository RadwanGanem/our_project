﻿namespace DanielAndRadwanProject_Client
{
    partial class BestScoresForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.ScoresListView = new System.Windows.Forms.ListView();
            this.nameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.scoreColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gamesPlayedLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(140, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(512, 120);
            this.Title.TabIndex = 24;
            this.Title.Text = "Daniel And Radwan\r\n              Triviya";
            // 
            // back
            // 
            this.back.Font = new System.Drawing.Font("Niagara Engraved", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.Black;
            this.back.Location = new System.Drawing.Point(290, 380);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(204, 58);
            this.back.TabIndex = 35;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // ScoresListView
            // 
            this.ScoresListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumn,
            this.scoreColumn});
            this.ScoresListView.Font = new System.Drawing.Font("Niagara Engraved", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScoresListView.Location = new System.Drawing.Point(175, 210);
            this.ScoresListView.Name = "ScoresListView";
            this.ScoresListView.Size = new System.Drawing.Size(437, 164);
            this.ScoresListView.TabIndex = 41;
            this.ScoresListView.UseCompatibleStateImageBehavior = false;
            // 
            // nameColumn
            // 
            this.nameColumn.Text = "Name";
            this.nameColumn.Width = 200;
            // 
            // scoreColumn
            // 
            this.scoreColumn.Text = "Score";
            this.scoreColumn.Width = 200;
            // 
            // gamesPlayedLabel
            // 
            this.gamesPlayedLabel.AutoSize = true;
            this.gamesPlayedLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gamesPlayedLabel.ForeColor = System.Drawing.Color.White;
            this.gamesPlayedLabel.Location = new System.Drawing.Point(294, 147);
            this.gamesPlayedLabel.Name = "gamesPlayedLabel";
            this.gamesPlayedLabel.Size = new System.Drawing.Size(153, 51);
            this.gamesPlayedLabel.TabIndex = 42;
            this.gamesPlayedLabel.Text = "Best Scores";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Niagara Engraved", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(635, 390);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(153, 51);
            this.usernameLabel.TabIndex = 43;
            this.usernameLabel.Text = "[USERNAME]";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // BestScoresForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.gamesPlayedLabel);
            this.Controls.Add(this.ScoresListView);
            this.Controls.Add(this.back);
            this.Controls.Add(this.Title);
            this.Name = "BestScoresForm";
            this.Text = "BestScoresForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.ListView ScoresListView;
        private System.Windows.Forms.ColumnHeader nameColumn;
        private System.Windows.Forms.ColumnHeader scoreColumn;
        private System.Windows.Forms.Label gamesPlayedLabel;
        private System.Windows.Forms.Label usernameLabel;
    }
}