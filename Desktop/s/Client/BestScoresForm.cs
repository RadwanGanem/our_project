﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielAndRadwanProject_Client
{
    public partial class BestScoresForm : Form
    {
        public static string scoresMsg { get; set; }
        public BestScoresForm()
        {
            InitializeComponent();
            usernameLabel.Text = LoggedInForm.userConnected;
            ScoresListView.View = System.Windows.Forms.View.Details;
            ScoresListView.FullRowSelect = true;
            int next = 0;
            for (int i = 0; i < 3; i++)
            {
                int nameLen = Convert.ToInt32(scoresMsg.Substring(3 + next, 2));
                string name = scoresMsg.Substring(5 + next, nameLen);
                string score = Convert.ToInt32(scoresMsg.Substring(5 + nameLen + next, 6)).ToString();
                string[] row = new string[] { name, score };
                ListViewItem item = new ListViewItem(row);
                ScoresListView.Items.Add(item);
                ScoresListView.Refresh();
                next = next + 8 + nameLen;
            }
        }
       
        private void back_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
