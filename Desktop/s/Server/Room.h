#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <queue>
#include <fstream>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include "User.h"

class User;

using namespace std;

class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
public:
	Room(int id, User* admin, string name, int maxUsers, int questionTime, int _questionNo);
	string getUsersListMessage();
	void sendMessage(User* excludeUser, string message);
	void sendMessage(string message);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	int getId();
	string getName();
	int getMaxUsers();
	vector<User*> getUsers();
	int getQuestionTime();
	int getQuestionNo();
};

