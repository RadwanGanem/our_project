#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET socket, int messageCode)
{
	this->_sock = socket;
	this->_messageCode = messageCode;
	this->_user = nullptr;
	if (messageCode == 203)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
	}
	if (messageCode == 200)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
	}
	if (messageCode == 213)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, Helper::getIntPartFromSocket(this->_sock, 2)));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 1));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 2));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 2));
	}
	if (messageCode == 205)
	{

	}
	if (messageCode == 207)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 4));
	}
	if (messageCode == 209)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 4));
	}
	if (messageCode == 211)
	{

	}
	if (messageCode == 215)
	{

	}
	if (messageCode == 201)
	{

	}
	if (messageCode == 217)
	{

	}
	if (messageCode == 219)
	{
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 1));
		this->_values.emplace_back(Helper::getStringPartFromSocket(this->_sock, 2));
	}
	if (messageCode == 222)
	{

	}
	if (messageCode == 225)
	{

	}
}

RecievedMessage::RecievedMessage(SOCKET socket, int messageCode, vector<string> values)
{
	this->_sock = socket;
	this->_messageCode = messageCode;
	this->_user = nullptr;
	this->_values = values;
}

SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User * newUser)
{
	this->_user = newUser;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}
