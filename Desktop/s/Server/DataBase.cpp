#include "DataBase.h"


unordered_map<string, vector<string>> results;

void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

DataBase::DataBase()
{
	/*this->_questions[0] = new Question(0,"What did the crocodile swallow in Peter Pan?", "alarm clock", "watch", "pirate", "hook the pirate");
	this->_questions[1] = new Question(1,"How many states are there in the United States of America?", "50", "49", "37", "16");
	this->_questions[2] = new Question(2, "When did the First World War start?", "1914", "1900", "1921", "1889");
	this->_questions[3] = new Question(3, "What did Joseph Priesley discover in 1774?", "Oxygen", "TelePhone", "TV", "Electricity");
	this->_questions[4] = new Question(4, "Where is the smallest bone in the body?", "ear", "nose", "leg", "finger");
	this->_questions[5] = new Question(5, "Which is the only mammal that can�t jump?", "elephant", "giraffe", "turtle", "shark");*/

	string st;
	int size;
	rc = sqlite3_open("trivia.db", &_db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		system("pause");
	}
	
	st = "select * from t_users";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	
	size = results["username"].size();
	for (int j = 0; j < size; j++)
	{
		_usernames.emplace_back(results["username"][j]);
		_passwords.emplace_back(results["password"][j]);
		_emails.emplace_back(results["email"][j]);
	}

	results.clear();

	st = "select * from t_questions";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);

	size = results["question"].size();
	for (int j = 0; j < size; j++)
	{

		_questions.emplace_back(new Question(stoi(results["question_id"][j]) , results["question"][j] , results["correct_ans"][j] , results["ans2"][j], results["ans3"][j], results["ans4"][j]));

	}

	results.clear();

	

}

string DataBase::maxResultUser(map<string, int> m)
{
	int max = 0;
	string maxUser;
	for (map<string, int>::const_iterator it = m.begin(); it != m.end(); it++)
	{
		if (it->second > max)
		{
			max = it->second;
			maxUser = it->first;
		}
	}

	return maxUser;
}


bool DataBase::fileExistsOnDisk(const std::string& filename)
{
	struct stat buffer;
	return (stat(filename.c_str(), &buffer) == 0);
}


DataBase::~DataBase()
{

}

bool DataBase::isUserExists(string username)
{

	for (int i = 0; i < this->_usernames.capacity(); i++)
	{
		if (username == _usernames[i])
		{
			return true;
		}
	}
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	string st = "INSERT INTO t_users (username, password, email) VALUES ( '" + username + "','" + password + "','" + email + "')";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		return false;
	}

	_usernames.emplace_back(username);
	_passwords.emplace_back(password);
	_emails.emplace_back(email);
	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	int j = -1;
	for (int i = 0; i < this->_usernames.capacity(); i++)
	{
		if (username == _usernames[i])
		{
			j = i;
			break;
		}
	}
	if (j != -1 && password == this->_passwords[j])
	{
		return true;
	}
	return false;
}

vector<Question*> DataBase::initQuestions(int questionNo)
{
	srand((unsigned)time(0));
	vector<Question*> question;
	int *index = new int[questionNo];
	bool boo = true;
	for (int i = 0; i < questionNo; i++)
	{
		boo = true;
		while (boo)
		{
			index[i] = rand() % questionNo;
			if (i == 0)
			{
				boo = false;
			}
			for (int j = 0; j < i; j++)
			{
				if (index[i] == index[j])
				{
					boo = true;
					break;
				}
				else
				{
					boo = false;
				}
			}
		}
	}
	for (int i = 0; i < questionNo; i++)
	{
		question.emplace_back(this->_questions[index[i]]);
	}
	return question;
}

int DataBase::insertNewGame()
{
	int id;
	results.clear();
	string st = "SELECT MAX(game_id) AS LastID FROM t_games";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	id = stoi(results["LastID"][0]);
	st = "INSERT INTO t_games(game_id,status,start_time,end_time) VALUES ('" + to_string(id + 1) + "','0', datetime('now') , '')";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	if (rc)
	{
		throw std::exception("Error while inserting to sql database!!!");
	}
	return id + 1;
}

bool DataBase::updateGameStatus(int id)
{
	results.clear();
	string st = "UPDATE t_games SET status = 1 , end_time = datetime('now') WHERE game_id = "+id;
	//"UPDATE t_games SET status = 1 , end_time = date('now') WHERE game_id = " + id;
	return !sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	results.clear();
	int boo;
	if (isCorrect)
	{
		boo = 1;
	}
	else
	{
		boo = 0;
	}
	string st = "INSERT INTO t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) VALUES ( '" + to_string(gameId) + "','" + username + "','" + to_string(questionId) + "','" + answer + "','" + to_string(boo) + "','" + to_string(answerTime) + "')";
	
	return !sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
}

string DataBase::getBestScores()
{
	results.clear();
	string st;
	int size;
	string msg = "124";
	string nameSize;
	string points;
	vector<string> maxScoresNames;
	vector<string> maxScoresPoints;
	pair<string, string> p;
	st = "SELECT * FROM t_players_answers";

	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	size = results["question"].size();
	map<string, int> finalResults;

	size = results["username"].size();
	for (int i = 0; i <size; i++)
	{
		if (finalResults.find(results["username"][i]) == finalResults.end())
		{
			finalResults.emplace(results["username"][i], 0);
		}
	}

	for (int i = 0; i < results["username"].size(); i++)
	{
		if (stoi(results["is_correct"][i]))
		{
			finalResults[results["username"][i]]++;
		}
	}
	//removing the max to get the next max
	int score;
	for (int i = 0; i < 3 && i < finalResults.size(); i++)
	{
		maxScoresNames.emplace_back(maxResultUser(finalResults));
		maxScoresPoints.emplace_back(to_string(finalResults[maxResultUser(finalResults)]));
		finalResults.erase(maxResultUser(finalResults));
	}

	for (int i = 0; i < maxScoresNames.size(); i++)
	{
		nameSize = to_string(maxScoresNames[i].size());
		while (nameSize.size() < 2)
		{
			nameSize = "0" + nameSize;
		}
		msg = msg + nameSize + maxScoresNames[i];
		points = maxScoresPoints[i];
		while (points.size() < 6)
		{
			points = "0" + points;
		}
		msg = msg + points;
	}
	for (int i = 0; i < 3 - maxScoresNames.size(); i++)
	{
		msg = msg + "00";
	}


	return msg;
}

string DataBase::getPersonalStatus(string username)
{
	map<string, int> s;
	string st;
	int size;
	int rightAns = 0;
	int wrongAns = 0;
	int sumTime = 0;
	int avgTime = 0;
	int numOfGames;
	st = "SELECT * FROM t_players_answers";
	rc = sqlite3_exec(_db, st.c_str(), callback, 0, &zErrMsg);
	size = results["game_id"].size();

	for (int i = 0; i < size; i++)
	{
		if (results["username"][i] == username)
		{
			if (stoi(results["is_correct"][i]) == 1)
			{
				rightAns++;
			}
			else
			{
				wrongAns++;
			}
			sumTime = sumTime + stoi(results["answer_time"][i]);
			s.emplace(results["game_id"][i], 0);
		}
	}
	numOfGames = s.size();
	//avgTime = ((int)(((double)sumTime / (rightAns + wrongAns))*100))/100.0;
	avgTime = ((double)sumTime / (rightAns + wrongAns)) * 100;

	string msg = "126";
	string ss;
	ss = to_string(numOfGames);
	while (ss.size() < 4)
	{
		ss = "0" + ss;
	}
	msg = msg + ss;

	ss = to_string(rightAns);
	while (ss.size() < 6)
	{
		ss = "0" + ss;
	}
	msg = msg + ss;

	ss = to_string(wrongAns);
	while (ss.size() < 6)
	{
		ss = "0" + ss;
	}
	msg = msg + ss;

	ss = to_string(avgTime);
	while (ss.size() < 4)
	{
		ss = "0" + ss;
	}
	msg = msg + ss;

	return msg;
}

int DataBase::callback(void * notUsed, int argc, char ** argv, char ** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{

		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];

			p.second.push_back(argv[i]);

			results.insert(p);
		}
	}

	return 0;
}
