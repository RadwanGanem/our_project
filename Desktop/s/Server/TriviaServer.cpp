#include "TriviaServer.h"
#include <exception>
#include <iostream>
#include <string>

using namespace std;

queue<string> myClients;
std::mutex m;
int cnt = 0;
fstream f;
std::condition_variable con;

string fileData = "";

TriviaServer::TriviaServer()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = ::socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

TriviaServer::~TriviaServer()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_serverSocket);
	}
	catch (...) {}
}

void TriviaServer::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"
	

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
	

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;

		accept();

	}
}


void TriviaServer::handleGetUsersInRoom(RecievedMessage * msg)
{
	int roomId = stoi(msg->getValues()[0]);
	Room* room = this->_roomList[roomId];
	Helper::sendData(msg->getSock(),room->getUsersListMessage());
}

void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	string st = "106";
	string roomIdSequence = to_string(this->_roomIdSequence-1);
	while (roomIdSequence.length() < 4)
	{
		roomIdSequence = "0" + roomIdSequence;
	}
	st = st + roomIdSequence;
	for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		roomIdSequence = to_string(it->first);
		while (roomIdSequence.length() < 4)
		{
			roomIdSequence = "0" + roomIdSequence;
		}
		st = st + roomIdSequence; 
		roomIdSequence = to_string(it->second->getName().length());
		while (roomIdSequence.length() < 2)
		{
			roomIdSequence = "0" + roomIdSequence;
		}
		st = st + roomIdSequence + it->second->getName();
	}
	Helper::sendData(msg->getSock(), st);
}

void TriviaServer::handleStartGame(RecievedMessage * msg)
{
	try
	{
		Game* game = new Game(msg->getUser()->getRoom()->getUsers(), msg->getUser()->getRoom()->getQuestionNo(), this->_db);

		for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
		{
			if (it->second == msg->getUser()->getRoom())
			{
				this->_roomList.erase(it->first);
				this->_roomIdSequence--;
				break;
			}
		}
		msg->getUser()->setGame(game);
		game->sendFirstQuestion();
	}
	catch (exception &e)
	{

	}

}

void TriviaServer::handlePlayerAnswer(RecievedMessage * msg)
{
	Game* game = msg->getUser()->getGame();
	vector<string> vec = msg->getValues();
	bool boo;
	
	if (game != nullptr)
	{
		boo = game->handleAnswerFromUser(msg->getUser(),atoi(vec[0].c_str()), atoi(vec[1].c_str()));
		/*if (boo == false)
		{
			game->~Game();
		}*/
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage * msg)
{
	if (msg->getUser()->leaveGame())
	{
		msg->getUser()->clearGame();
	}

}

void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
	msg->getUser()->send(this->_db.getBestScores());
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * msg)
{
	msg->getUser()->send(this->_db.getPersonalStatus(msg->getUser()->getUsername()));
}

void TriviaServer::safeDeleteUser(RecievedMessage * msg)
{
	handleSignout(msg);
	closesocket(msg->getSock());
}

void TriviaServer::handleSignout(RecievedMessage * msg)
{
	if (msg->getUser() != nullptr)
	{
		this->_connectedUsers.erase(msg->getSock());

		if (msg->getUser()->getRoom() != nullptr)
		{
			handleCloseRoom(msg);
		}
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}
	/*if (msg->getUser() != nullptr)
	{
		this->_connectedUsers.erase(msg->getSock());
	}*/
	//sleep_until(system_clock::now() + seconds(10));

}

void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;
	std::thread *t = new std::thread[200];
	// the function that handle the conversation with the clients with threads detached

	t[cnt] = std::thread(&TriviaServer::clientHandler, this, client_socket);
	cnt++;
	//detaching
	t[cnt-1].detach();

	
	
}


void TriviaServer::clientHandler(SOCKET clientSocket)
{
	int MTC = 0;
	string n = "";
	string data;
	int num;
	string st;
	string currUser;
	string nextUser;
	int position;
	queue<string> temp;
	std::unique_lock<std::mutex> fLock(m,defer_lock);

	while (clientSocket)
	{
		try
		{
			MTC = Helper::getMessageTypeCode(clientSocket);
			RecievedMessage* rcv = new RecievedMessage(clientSocket,MTC);
			rcv->setUser(getUserBySocket(rcv->getSock()));
			
			//sleep_until(system_clock::now() + seconds(1));

			addRecievedMessage(rcv);
			handleRecievedMessages();
		}
		catch (const std::exception& e)
		{
			//closesocket(clientSocket);
		}
	}
}

// get the first user
// i used another queue to save first queue positions
string TriviaServer::getCurrUser(queue<string> users)
{
	queue<string> temp = users;
	return temp.front();
}

// get the second user and pop the first 
// i used another queue to save first queue positions
string TriviaServer::getNextUser(queue<string> users)
{
	queue<string> temp = users;
	temp.pop();
	if (temp.size() == 0)
		return "";
	return temp.front();
}
// get the user position in the queue
int TriviaServer::getUserPosition(queue<string> users, string name)
{
	queue<string> temp = users;
	int cnt = 1;
	while (temp.front() != name)
	{
		temp.pop();
		cnt++;
	}
	return cnt;
}
// delete an user by username from the queue
void TriviaServer::deleteClient(queue<string> users, string name)
{
	queue<string> temp1 = users;
	queue<string> temp2;
	while (temp1.size())
	{
		if (temp1.front() != name)
		{
			temp2.push(temp1.front());
		}
		temp1.pop();
	}
	while (temp2.size())
	{
		temp1.push(temp2.front());
		temp2.pop();
	}
	myClients = temp1;
}


void TriviaServer::addRecievedMessage(RecievedMessage * rcvMessage)
{
	_queRcvMessages.push(rcvMessage);
}

RecievedMessage * TriviaServer::buildRecievedMessage(SOCKET socket, int messageCode)
{
	return new RecievedMessage(socket, messageCode);
}


Room * TriviaServer::getRoomById(int id)
{
	return this->_roomList[id];
}

User * TriviaServer::getUserByName(string name)
{
	for (auto& x : _connectedUsers)
	{
		if (x.second->getUsername() == name)
		{
			return x.second;
		}
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET socket)
{
	for (auto& x : this->_connectedUsers)
	{
		if (x.first == socket)
		{
			return x.second;
		}
	}
	return nullptr;
}

void TriviaServer::handleRecievedMessages()
{
	//std::unique_lock<std::mutex> fLock(m, defer_lock);
	RecievedMessage* rcv = _queRcvMessages.front();
	_queRcvMessages.pop();
	//fLock.unlock();
	User* user = getUserBySocket(rcv->getSock());
	vector<string> vec = rcv->getValues();
	string message = "";
	if (rcv->getMessageCode() == 200)
	{
		handleSignin(rcv);
	}
	else if (rcv->getMessageCode() == 203)
	{
		handleSignup(rcv);
	}
	else if (rcv->getMessageCode() == 213)
	{
		handleCreateRoom(rcv);
	}
	else if (rcv->getMessageCode() == 205)
	{
		handleGetRooms(rcv);
	}
	else if (rcv->getMessageCode() == 207)
	{
		handleGetUsersInRoom(rcv);
	}
	else if (rcv->getMessageCode() == 209)
	{
		handleJoinRoom(rcv);
	}
	else if (rcv->getMessageCode() == 211)
	{
		handleLeaveRoom(rcv);
	}
	else if (rcv->getMessageCode() == 215)
	{
		handleCloseRoom(rcv);
	}
	else if (rcv->getMessageCode() == 201)
	{
		handleSignout(rcv);
	}
	else if (rcv->getMessageCode() == 217)
	{
		handleStartGame(rcv);
	}
	else if (rcv->getMessageCode() == 219)
	{
		handlePlayerAnswer(rcv);
	}
	else if (rcv->getMessageCode() == 222)
	{
		handleLeaveGame(rcv);
	}
	else if (rcv->getMessageCode() == 223)
	{
		handleGetBestScores(rcv);
	}
	else if (rcv->getMessageCode() == 225)
	{
		handleGetPersonalStatus(rcv);
	}
	else
	{
		safeDeleteUser(rcv);
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	vector<string> vec = msg->getValues();
	if (user != nullptr)
	{
		if (user->createRoom(_roomIdSequence, vec[0], stoi(vec[1]), stoi(vec[2]), stoi(vec[3])))
		{
			this->_roomList.emplace(_roomIdSequence, user->getRoom());
			_roomIdSequence++;
			return true;
		}
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	int roomId = user->closeRoom();
	if (roomId != -1)
	{
		this->_roomList.erase(roomId);
		this->_roomIdSequence--;
		return true;
	}
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	int roomId = stoi(msg->getValues()[0]);
	Room* room = getRoomById(roomId);
	if (user != nullptr)
	{
		if (room != nullptr)
		{
			if ( room->getMaxUsers() > (int)room->getUsers().size())
			{
				user->joinRoom(room);
				return room->joinRoom(user);
			}
			else
			{
				Helper::sendData(msg->getSock(), "1101");
				return false;
			}
		}
	}
	Helper::sendData(msg->getSock(), "1102");
	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	user->leaveRoom();
	return true;
}



User* TriviaServer::handleSignin(RecievedMessage * msg)
{
	vector<string> vec = msg->getValues();
	User* newUser = nullptr;
	if (this->_db.isUserAndPassMatch(vec[0], vec[1]))
	{
		newUser = getUserByName(vec[0]);
		if (newUser == nullptr)
		{
			Helper::sendData(msg->getSock(), "1020");
			newUser = new User(vec[0], msg->getSock());
			this->_connectedUsers.emplace(msg->getSock(), newUser);
			msg->setUser(newUser);
		}
		else
		{
			Helper::sendData(msg->getSock(), "1022");
		}
	}
	else
	{
		Helper::sendData(msg->getSock(), "1021");
	}

	return newUser;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	vector<string> vec = msg->getValues();
	if (Validator::isUsernameValid(vec[0]))
	{
		if (Validator::isPasswordValid(vec[1]))
		{
			if (!this->_db.isUserExists(vec[0]))
			{
				this->_db.addNewUser(vec[0], vec[1], vec[2]);
				Helper::sendData(msg->getSock(), "1040");
				return true;
			}
			else
			{
				Helper::sendData(msg->getSock(), "1042");
			}
		}
		else
		{
			Helper::sendData(msg->getSock(), "1041");
		}
	}
	else
	{
		Helper::sendData(msg->getSock(), "1043");
	}
	return false;

}