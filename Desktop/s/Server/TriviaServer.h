#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <queue>
#include <fstream>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include "User.h"
#include "Room.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Validator.h"

#define FILE_CONTENT_SIZE 5
#define USERNAME_SIZE 2

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve(int port);
	string getCurrUser(queue<string> users);
	string getNextUser(queue<string> users);
	int getUserPosition(queue<string> users, string name);
	void deleteClient(queue<string> users, string name);

	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);

	void addRecievedMessage(RecievedMessage* rcvMessage);
	RecievedMessage* buildRecievedMessage(SOCKET socket, int messageCode);

	Room* getRoomById(int id);
	User* getUserByName(string name);
	User* getUserBySocket(SOCKET socket);

	void handleRecievedMessages();

	bool handleCreateRoom(RecievedMessage* msg);//213
	bool handleCloseRoom(RecievedMessage* msg);//215
	bool handleJoinRoom(RecievedMessage* msg);//209
	bool handleLeaveRoom(RecievedMessage* msg);//211
	void handleGetUsersInRoom(RecievedMessage* msg);//207
	void handleGetRooms(RecievedMessage* msg);//205
	void handleStartGame(RecievedMessage* msg);//217
	void handlePlayerAnswer(RecievedMessage* msg);//219
	void handleLeaveGame(RecievedMessage* msg);//222
	void handleGetBestScores(RecievedMessage* msg);//223
	void handleGetPersonalStatus(RecievedMessage* msg);//225
	void safeDeleteUser(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg); // 201

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	map<SOCKET, User*> _connectedUsers;
	map<int, Room*> _roomList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	int _roomIdSequence = 1;
	DataBase _db;

	SOCKET _serverSocket;
	
};

