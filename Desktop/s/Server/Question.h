#pragma once
#include <iostream>
#include <string>
#include <cstdlib> 
#include <ctime>

class Question
{
private:
	std::string _question;
	std::string _answers[4];
	int _currentAnswerIndex;
	int _id;
public:
	Question(int id,std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4);
	std::string getQuestion();
	std::string* getAnswer();
	int getCurrentAnswerIndex();
	int getId();
};

