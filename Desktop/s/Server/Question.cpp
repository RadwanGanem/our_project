#include "Question.h"

Question::Question(int id,std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4)
{
	this->_id = id;
	this->_question = question;
	srand((unsigned)time(0));
	bool boo = true;
	int index[4];
	index[0] = rand() % 4;
	while (boo)
	{
		index[1] = rand() % 4;
		if (index[1] != index[0])
		{
			boo = false;
		}
	}
	boo = true;
	while (boo)
	{
		index[2] = rand() % 4;
		if (index[2] != index[0] && index[2] != index[1])
		{
			boo = false;
		}
	}
	boo = true;
	while (boo)
	{
		index[3] = rand() % 4;
		if (index[3] != index[0] && index[3] != index[1] && index[3] != index[2])
		{
			boo = false;
		}
	}
	this->_answers[index[0]] = correctAnswer;
	this->_answers[index[1]] = answer2;
	this->_answers[index[2]] = answer3;
	this->_answers[index[3]] = answer4;
	this->_currentAnswerIndex = index[0];
}

std::string Question::getQuestion()
{
	return this->_question;
}

std::string * Question::getAnswer()
{
	return this->_answers;
}

int Question::getCurrentAnswerIndex()
{
	return this->_currentAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}
