#pragma once

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <thread>
#include <chrono>

#include "User.h"
#include "DataBase.h"
#include "Question.h"

class User;

using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono; // nanoseconds, system_clock, seconds


class Game
{
private:
	std::map<std::string, int> _results;
	int _currentTurnAnswers;
	std::vector<User*> _players;
	int _quiestion_no;
	int _currQuiestionIndex;
	DataBase _db;
	std::vector<Question*> _questions;
	int _id;
public:
	Game(std::vector<User*> players, int questionNo, DataBase db);
	~Game();
	bool leaveGame(User* player);
	void sendFirstQuestion();
	void sendQuestionToAllUsers();
	bool handleAnswerFromUser(User* user, int answerNo,int time);
	bool handleNextTurn();
	void handleFinishGame();
	int getId();
};

