#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>
#include "sqlite3.h"
#include <string.h>
#include <sstream>
#include "Question.h"
#include <cstdlib> 
#include <ctime>
#include <map>
#include <math.h>

using std::string;
using std::vector;
using std::unordered_map;
using std::cout;
using std::endl;
using std::map;
using namespace std;

class DataBase
{
private:
	
	sqlite3 * _db;
	vector<string> _usernames;
	vector<string> _passwords;
	vector<string> _emails;
	vector<Question*> _questions;
	
	void clearTable();
	int rc;
	char *zErrMsg = 0;
	bool flag = true;

public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	bool fileExistsOnDisk(const std::string& filename);

	vector<Question*> initQuestions(int questionNo);
	int insertNewGame();
	bool updateGameStatus(int id);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	string getBestScores();
	string getPersonalStatus(string username);
	//static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	string maxResultUser(map<string, int> m);
	//static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	//static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	//static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);
};

