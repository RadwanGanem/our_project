#include "Game.h"

Game::Game(std::vector<User*> players, int questionNo, DataBase db)
{
	this->_players = players;
	this->_db = db;
	this->_quiestion_no = questionNo;
	this->_currQuiestionIndex = 0;
	for (int i = 0; i < players.size(); i++)
	{
		this->_results.emplace(players[i]->getUsername(), 0);
	}
	this->_questions = this->_db.initQuestions(this->_quiestion_no);
	try
	{
		_id = _db.insertNewGame();
	}
	catch (std::exception &e)
	{
		cout << &e << endl;
	}
	this->_questions = this->_db.initQuestions(this->_quiestion_no);
}

Game::~Game()
{

}

bool Game::leaveGame(User * player)
{
	for (int i = 0; i < _players.size(); i++)
	{
		if (_players[i] == player)
		{
			_players.erase(_players.begin() + i);
			return handleNextTurn();
		}
	}
	
	return false;
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::sendQuestionToAllUsers()
{
	this->_currentTurnAnswers = 0;
	string st = "118";
	if (this->_questions[this->_currQuiestionIndex]->getQuestion().size() > 0)
	{
		string size = to_string(this->_questions[this->_currQuiestionIndex]->getQuestion().size());
		while (size.size() < 3)
		{
			size = "0" + size;
		}
		st = st + size + this->_questions[this->_currQuiestionIndex]->getQuestion();
		std::string* answers = this->_questions[this->_currQuiestionIndex]->getAnswer();
		for (int i = 0; i < 4; i++)
		{
			size = to_string(answers[i].size());
			while (size.size() < 3)
			{
				size = "0" + size;
			}
			st = st + size + answers[i];
		}
		for (int i = 0; i < this->_players.size(); i++)
		{
			try
			{
				this->_players[i]->send(st);
			}
			catch (std::exception &e)
			{
				cout << &e << endl;
			}
		}
	}
	else
	{
		for (int i = 0; i < this->_players.size(); i++)
		{
			this->_players[i]->send("1180");
		}
	}
}

bool Game::handleAnswerFromUser(User * user, int answerNo, int time)
{
	this->_currentTurnAnswers++;
	if (_questions[_currQuiestionIndex]->getCurrentAnswerIndex() == answerNo-1)
	{
		this->_results[user->getUsername()]++;
		user->send("1201");
		this->_db.addAnswerToPlayer(_id, user->getUsername(), _currQuiestionIndex, _questions[_currQuiestionIndex]->getAnswer()[answerNo], true, time);

	}
	else
	{
		if (answerNo == 5)
		{
			this->_db.addAnswerToPlayer(_id, user->getUsername(), _currQuiestionIndex, NULL, false, time);
		}
		else
		{
			this->_db.addAnswerToPlayer(_id, user->getUsername(), _currQuiestionIndex, _questions[_currQuiestionIndex]->getAnswer()[answerNo], false, time);
		}
		user->send("1200");
	}

	//sleep_until(system_clock::now() + seconds(1));
	return handleNextTurn();
}

bool Game::handleNextTurn()
{
	if (this->_players.size() > 0)
	{
		if (this->_currentTurnAnswers == this->_players.size())
		{
			if (this->_currQuiestionIndex < this->_quiestion_no-1)
			{
				this->_currQuiestionIndex++;
				sendQuestionToAllUsers();
				return true;
			}
			else
			{
				handleFinishGame();
				return false;
			}
		}
		return true;
	}
	else
	{
		handleFinishGame();
		return false;
	}
}

void Game::handleFinishGame()
{
	this->_db.updateGameStatus(this->_id);
	string st = "121" + to_string(this->_players.size());
	for (int i = 0; i < this->_players.size(); i++)
	{
		string size = to_string(this->_players[i]->getUsername().size());
		while (size.size() < 2)
		{
			size = "0" + size;
		}
		st = st + size + this->_players[i]->getUsername() + to_string(_results[this->_players[i]->getUsername()]);
	}
	for (int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			_players[i]->send(st);
			//_players[i]->setGame(nullptr);
			_players[i]->leaveGame();
			_players[i]->clearGame();
		}
		catch (std::exception &e)
		{

		}
	}

}

int Game::getId()
{
	return _id;
}
