#include "Room.h"



Room::Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionNo)
{
	this->_id = id;
	this->_admin = admin;
	this->_maxUsers = maxUsers;
	this->_name = name;
	this->_questionTime = questionTime;
	this->_questionNo = questionNo;
	this->_users.emplace_back(admin);
}

string Room::getUsersListMessage()
{
	string st = "";
	st = "108" + st + to_string(this->_users.size());
	string s;
	for (int i = 0; i < this->_users.size(); i++)
	{
		s = to_string(this->_users[i]->getUsername().length());
		while (s.length() < 2)
		{
			s = "0" + s;
		}
		st = st + s + (this->_users[i]->getUsername());
	}
	return st;

}

void Room::sendMessage(User * excludeUser, string message)
{
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != excludeUser)
		{
			this->_users[i]->send(message);
		}
	}
}

void Room::sendMessage(string message)
{
	this->sendMessage(nullptr, message);
}

bool Room::joinRoom(User * user)
{
	if (this->_users.size() < this->_maxUsers)
	{
		this->_users.emplace_back(user);
		string st = "1100";
		string questionData = to_string(this->_questionNo); // change
		while (questionData.length() < 2)
		{
			questionData = "0" + questionData;
		}
		st = st + questionData;
		questionData = to_string(this->_questionTime); // change 
		while (questionData.length() < 2)
		{
			questionData = "0" + questionData;
		}
		st = st + questionData;
		user->send(st);
		for (int i = 0; i < this->_users.size(); i++)
		{
			this->sendMessage(this->getUsersListMessage());
		}
		return true;
	}
	else
	{
		return false;
	}
}

void Room::leaveRoom(User * user)
{
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == user)
		{
			this->_users[i]->send("1120");
			this->_users.erase(this->_users.begin() + i);
		}
	}

	for (int i = 0; i < this->_users.size(); i++)
	{
		this->sendMessage(this->getUsersListMessage());
	}
}

int Room::closeRoom(User * user)
{
	if (this->_admin == user)
	{
		for (int i = 0; i < this->_users.size(); i++)
		{
			/*if (this->_users[i] != this->_admin)
			{
				this->_users[i]->clearRoom();
			}*/
			this->_users[i]->clearRoom();
			this->_users[i]->send("116");
		}
		
		return this->_id;
	}
	else
	{
		return -1;
	}
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}

int Room::getMaxUsers()
{
	return this->_maxUsers;
}

vector<User*> Room::getUsers()
{
	return this->_users;
}

int Room::getQuestionTime()
{
	return this->_questionTime;
}

int Room::getQuestionNo()
{
	return this->_questionNo;
}
