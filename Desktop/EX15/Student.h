/* version 1.0 of my school*/

#include <iostream>
#include <string>

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;

class Student
{
public:
	void init(string name, Course** courses, int crsCount);

	string getName();
	void setName(string name);
	int getCrsCount();
	Course** getCourses();
	double getAvg();


private:
	string _name;
	Course** _Courses;//array of pointers to Course
	int _crsCount;
};